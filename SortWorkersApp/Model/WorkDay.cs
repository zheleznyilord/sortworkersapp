﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SortWorkersApp.Model
{
    public class WorkDay
    {
        public string Name { get; set; }
        public string Date { get; set; }
        public double Hours { get; set; }

    }
}
