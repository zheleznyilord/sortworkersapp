﻿using SortWorkersApp.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortWorkersApp.Service
{
    public class FileService
    {

        public async Task<List<WorkDay>> ReadFromCSVAsync(string path)
        {
            var days = new List<WorkDay>();
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    var file = await sr.ReadToEndAsync();
                    var lines = file.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    lines = lines.Skip(1).ToArray();
                    foreach (var line in lines)
                    {
                        var fields = line.Split(',');
                        var fl = double.Parse(fields[2], CultureInfo.InvariantCulture);
                        var day = new WorkDay
                        {
                            Name = fields[0],
                            Date = fields[1],
                            Hours = fl
                        };
                        days.Add(day);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return days;
        }


        public void SaveToCSV(List<WorkDay> days, string path)
        {
            StringBuilder sb = generateFileText(days);
            try
            {
                using (StreamWriter sw = new StreamWriter(path))
                {
                    sw.Write(sb);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static StringBuilder generateFileText(List<WorkDay> days)
        {
            StringBuilder sb = new StringBuilder();

            var dates = days.Select(d => d.Date).Distinct().ToList();
            sb.Append("Name/Date,");
            dates.ForEach(d => sb.Append(d + ","));
            sb.Append("\r\n");
            var names = days.Select(d => d.Name).Distinct().OrderBy(s => s).ToList();
            foreach (var name in names)
            {
                sb.Append(name);
                sb.Append(",");
                foreach (var date in dates)
                {
                    var hoursIsZero = true;
                    foreach (var day in days)
                    {
                        if (day.Name.Equals(name) && day.Date.Equals(date))
                        {
                            sb.Append(day.Hours.ToString().Replace(',', '.'));
                            sb.Append(",");
                            hoursIsZero = false;
                            break;
                        }
                    }
                    if (hoursIsZero)
                    {
                        sb.Append(0);
                        sb.Append(",");
                    }
                }
                sb.Append("\r\n");
            }
            return sb;
        }
    }
}
