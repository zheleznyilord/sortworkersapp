﻿using SortWorkersApp.Service;
using System;

namespace SortWorkersApp
{
    class Program
    {
        static void Main(string[] args)
        {
            FileService fileService = new FileService();
            var list = fileService.ReadFromCSVAsync("../../../acme_worksheet.csv").GetAwaiter().GetResult();
            fileService.SaveToCSV(list, "../../../create.csv");
        }
    }
}
